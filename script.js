const themeButton = document.getElementById('theme-switch')
const link = document.getElementById('theme')
const DARKCSS = 'css/dark.css'
if(localStorage.getItem('theme')){
    themeButton.checked = true
    link.href = localStorage.getItem('theme')
}

themeButton.addEventListener('click', (event)=>{
    if(event.target.checked){
        localStorage.setItem('theme' , DARKCSS)
    }
    else{
        localStorage.setItem('theme' , '')
    }
    link.href = localStorage.getItem('theme')
})

// 1. localStorage може бути видалена тільки вручну, sessionStorage видаляєтся після оновлення/закриття сторінки
// 2. мабуть ця інформація може бути легко знайдена якщи просто записувати ії в сторедж
// 3. вона видаляється